package main

import (
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/google/uuid"

	"github.com/gin-gonic/gin"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

const dbName = "csgo-vote-db"
const voteCollName = "votes"
const voteTimeout = 24 * time.Hour

var votesColl *mgo.Collection
var stopCleanupChannel chan bool
var hostAddr = flag.String("host", "localhost:4242", "host address")
var dbAddr = flag.String("db", "localhost:27017", "database address")
var debug bool

func main() {
	flag.BoolVar(&debug, "debug", false, "debug mode")
	flag.Parse()
	fmt.Println("debug: ", debug)

	f, err := os.OpenFile("csgo_vote_api.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	log.Print(err)
	log.SetOutput(io.MultiWriter(f))
	log.Print("Starting up the API...")

	dbSession, err := mgo.Dial(*dbAddr)
	if err != nil {
		log.Fatal(err)
	}

	votesColl = dbSession.DB(dbName).C(voteCollName)

	go func() {
		for range time.Tick(voteTimeout) {
			dbCleanup()
		}
	}()

	if !debug {
		gin.SetMode(gin.ReleaseMode)
	}
	gin.DefaultWriter = io.MultiWriter(f)
	router := gin.New()
	router.Use(gin.Recovery())
	if debug {
		router.Use(gin.Logger())
	}
	router.POST("/:voteId", joinVote)
	router.POST("/:voteId/:action", doVote)
	router.POST("/", createVote)

	router.Run(*hostAddr)
}

func joinVote(c *gin.Context) {
	identifier := strings.ToUpper(c.Param("voteId"))
	playerIdString := c.Query("playerId")
	playerName := c.Query("playerName")

	vote := Vote{}
	err := votesColl.Find(bson.M{"identifier": identifier}).One(&vote)
	if err != nil {
		sendOnError(c, err)
		return
	}

	if vote.Player2.Identifier != "" {
		sendOnError(c, errors.New("vote already full"))
		return
	}

	playerId, err := uuid.Parse(playerIdString)
	if err != nil {
		sendOnError(c, err)
		return
	}

	vote.Player2 = Player{
		Identifier: playerId.String(),
		Name:       playerName,
	}

	err = votesColl.UpdateId(vote.Id, vote)
	if err != nil {
		sendOnError(c, err)
		return
	}

	c.JSON(http.StatusOK, vote)
}

func createVote(c *gin.Context) {
	playerIdString := c.Query("playerId")
	playerName := c.Query("playerName")

	idString := makeIdentifier()

	var valid = false
	for valid == false {
		n, _ := votesColl.Find(bson.M{"identifier": idString}).Limit(1).Count()
		if n > 0 {
			log.Print("identifier already exists, generating a new one")
			valid = false
			idString = makeIdentifier()
		} else {
			valid = true
		}
	}

	playerId, err := uuid.Parse(playerIdString)
	if err != nil {
		sendOnError(c, err)
		return
	}

	player := Player{
		Identifier: playerId.String(),
		Name:       playerName,
	}

	vote := Vote{
		Id:         bson.NewObjectId(),
		Identifier: idString,
		Maps:       defaultMaps,
		PlayerTurn: player.Identifier,
		Player1:    player,
	}

	err = votesColl.Insert(vote)
	if err != nil {
		sendOnError(c, err)
		return
	}

	c.JSON(http.StatusOK, vote)
}

func doVote(c *gin.Context) {
	identifier := strings.ToUpper(c.Param("voteId"))
	action := c.Param("action")
	csMap := c.Query("map")
	playerId := c.Query("playerId")

	vote := Vote{}
	err := votesColl.Find(bson.M{"identifier": identifier}).One(&vote)
	if err != nil {
		sendOnError(c, err)
		return
	}

	if playerId != vote.PlayerTurn {
		sendOnError(c, errors.New("wrong player trying to make a turn"))
		return
	}

	switch action {
	case "in":
		err = vote.voteIn(csMap)
		if err != nil {
			sendOnError(c, err)
			return
		}
		break
	case "out":
		err = vote.voteOut(csMap)
		if err != nil {
			sendOnError(c, err)
			return
		}
		break
	default:
		sendOnError(c, errors.New("unrecognized vote action"))
		return
	}

	if vote.PlayerTurn == vote.Player1.Identifier {
		vote.PlayerTurn = vote.Player2.Identifier
	} else {
		vote.PlayerTurn = vote.Player1.Identifier
	}

	err = votesColl.UpdateId(vote.Id, vote)
	if err != nil {
		sendOnError(c, err)
		return
	}

	c.JSON(http.StatusOK, vote)
}

func sendOnError(c *gin.Context, e error) {
	if e == nil {
		return
	}
	if debug {
		c.JSON(http.StatusInternalServerError, gin.H{"error": e.Error()})
		log.Print(c.Request.Method + c.Request.RequestURI + " error " + e.Error())
	} else {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "something went wrong"})
	}
}

func makeIdentifier() string {
	var charString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	var chars = strings.Split(charString, "")

	var builder strings.Builder
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < 5; i++ {
		builder.Write([]byte(chars[rand.Intn(len(chars))]))
	}

	return builder.String()
}

func dbCleanup() {
	log.Print("performing database cleanup")
	n := 0
	vote := Vote{}
	iterator := votesColl.Find(bson.M{}).Iter()

	for iterator.Next(&vote) {
		created := vote.Id.Time()
		if time.Since(created) > voteTimeout {
			n++
			err := votesColl.RemoveId(vote.Id)
			if err != nil {
				log.Print(err)
			}
		}
	}
	log.Printf("deleted %d old votes", n)
}
