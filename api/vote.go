package main

import (
	"errors"

	"github.com/globalsign/mgo/bson"
)

var defaultMaps = []CSMap{
	CSMap{Name: "de_dust2", VotedOut: false, VotedIn: false},
	CSMap{Name: "de_inferno", VotedOut: false, VotedIn: false},
	CSMap{Name: "de_mirage", VotedOut: false, VotedIn: false},
	CSMap{Name: "de_nuke", VotedOut: false, VotedIn: false},
	CSMap{Name: "de_overpass", VotedOut: false, VotedIn: false},
	CSMap{Name: "de_train", VotedOut: false, VotedIn: false},
	CSMap{Name: "de_vertigo", VotedOut: false, VotedIn: false},
}

type Vote struct {
	Id         bson.ObjectId `json:"id" bson:"_id"`
	Identifier string        `json:"identifier" bson:"identifier"`
	PlayerTurn string        `json:"playerTurn" bson:"playerTurn"`
	Maps       []CSMap       `json:"maps" bson:"maps"`
	Player1    Player        `json:"player1" bson:"player1"`
	Player2    Player        `json:"player2" bson:"player2"`
}

type CSMap struct {
	Name       string `json:"name" bson:"name"`
	VotedOut   bool   `json:"votedOut" bson:"votedOut"`
	VotedIn    bool   `json:"votedIn" bson:"votedIn"`
	VotedInBy  string `json:"votedInBy" bson:"votedInBy"`
	VotedOutBy string `json:"votedOutBy" bson:"votedOutBy"`
}

type Player struct {
	Identifier string `json:"identifier" bson:"identifier"`
	Name       string `json:"name" bson:"name"`
}

func (v *Vote) voteIn(name string) error {
	for i, csMap := range v.Maps {
		if csMap.Name == name {
			if v.Maps[i].VotedIn || v.Maps[i].VotedOut {
				return errors.New("map already voted in or out")
			}
			v.Maps[i].VotedIn = true
			v.Maps[i].VotedInBy = v.PlayerTurn
			return nil
		}
	}
	return errors.New("map not found in vote")
}

func (v *Vote) voteOut(name string) error {
	for i, csMap := range v.Maps {
		if csMap.Name == name {
			if v.Maps[i].VotedIn || v.Maps[i].VotedOut {
				return errors.New("map already voted in or out")
			}
			v.Maps[i].VotedOut = true
			v.Maps[i].VotedOutBy = v.PlayerTurn
			return nil
		}
	}
	return errors.New("map not found in vote")
}
